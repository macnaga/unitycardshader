﻿Shader "Unlit/CardShader"
{
    Properties
    {
        _CardTex ("Card Texture", 2D) = "white" {}
        _CardMaskTex ("Card Mask Texture", 2D) = "white" {}

        [Space]
        [KeywordEnum(None,Scroll,Rotate)] _MoveType("Move Type",float) = 0
        _ScrollX("Scroll X",float) = 0
        _ScrollY("Scroll Y",float) = 0

        [Space]
        [Toggle] _Distortion("Distortion",float) = 0
        _DistortionTex ("Distortion Texture(RG)", 2D)            = "grey" {}
        _DistortionPowerX ("Distortion PowerX", Float)    = 0
        _DistortionPowerY ("Distortion PowerY", Float)    = 0
        _DistortionSpeed ("Distortion Speed", Float)    = 1

        [Toggle] _Distortion2("Distortion2",float) = 0
        _DistortionTex2 ("Distortion Texture2(RG)", 2D)            = "grey" {}
        _DistortionMaskTex2 ("Distortion Mask Texture2(RG)", 2D)            = "grey" {}
        _DistortionPowerX2 ("Distortion PowerX2", Float)    = 0
        _DistortionPowerY2 ("Distortion PowerY2", Float)    = 0
        _DistortionSpeed2 ("Distortion Speed2", Float)    = 1

        _DistortionScrollX("Dist Scroll X",float) = 0
        _DistortionScrollY("Dist Scroll Y",float) = 0
    }
    SubShader
    {
        Pass
        {
           Tags { "RenderType"="Opaque" }
            LOD 100
		    BlendOp Add
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            #pragma shader_feature _ _MOVETYPE_SCROLL _MOVETYPE_ROTATE
            #pragma shader_feature _DISTORTION_ON
            #pragma shader_feature _DISTORTION2_ON

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _CardTex;
            float4 _CardTex_ST;
            sampler2D _CardMaskTex;
            float4 _CardMaskTex_ST;
            float _ScrollX;
            float _ScrollY;
            sampler2D _DistortionTex;
            half4 _DistortionTex_ST;
            sampler2D _DistortionMaskTex2;
            half4 _DistortionMaskTex2_ST;
            half _DistortionPowerX;
            half _DistortionPowerY;
            half _DistortionSpeed;

            sampler2D _DistortionTex2;
            half4 _DistortionTex2_ST;
            half _DistortionPowerX2;
            half _DistortionPowerY2;
            half _DistortionSpeed2;
            float _DistortionScrollX;
            float _DistortionScrollY;
			
            float2 UV_RotateAround(float2 center,float2 uv,float rad)
			{
				float2 fuv = uv - center;
				float2x2 ma = float2x2(cos(rad),sin(rad),-sin(rad),cos(rad));
				fuv = mul(ma,fuv)+center;
				return fuv;
			}

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _CardTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 finalCol = fixed4(0,0,0,0);
                // tex2D(_CardTex, i.uv);

                fixed4 cardMaskCol = tex2D(_CardMaskTex, i.uv);
                //ひずみテクスチャのUV移動とテクスチャ合成
                fixed2 uv = i.uv;

                #ifdef _MOVETYPE_ROTATE
                uv = UV_RotateAround(fixed2(0.5,0.5),uv,(_ScrollX * _Time.y));    
                #elif _MOVETYPE_SCROLL
                uv += _Time.y * fixed2( _ScrollX , _ScrollY );
                #endif

                #ifdef _DISTORTION_ON
                half2 distortion    = tex2D(_DistortionTex,uv).rg - 0.5;
                distortion          *= half2( _DistortionPowerX * cos( _DistortionSpeed *_Time.y ),_DistortionPowerY * sin( _DistortionSpeed * _Time.y));
                uv += distortion;
                #endif
                
                fixed4 col = tex2D(_CardTex,uv);
                col *= cardMaskCol.b;
                finalCol += col/2;
                
                #ifdef _DISTORTION2_ON
                half2 distortion2    = tex2D(_DistortionTex2,uv).rg - 0.5;
                distortion2          *= half2( _DistortionPowerX2 * cos( _DistortionSpeed2 *_Time.y ),_DistortionPowerY2 * sin( _DistortionSpeed2 * _Time.y));
                half2 uv2 = i.uv;
                uv2 += distortion2;
                uv2 += _Time.y * fixed2( _DistortionScrollX , _DistortionScrollY);
                
                fixed4 distMaskTex2Col = tex2D( _DistortionMaskTex2 ,i.uv);
                // uv2 += _Time.y * fixed2( _ScrollX , _ScrollY );
                fixed4 col2 = tex2D( _DistortionTex2,uv2);
                col2 *= distMaskTex2Col.r;
                col2.a *= col2.rgb;
                
                finalCol += col2;
                #endif

                finalCol += tex2D(_CardTex, i.uv);
                return finalCol;
            }
            ENDCG
        }
    }
}
