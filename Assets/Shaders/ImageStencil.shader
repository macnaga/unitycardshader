﻿Shader "ImageStencil"
{
    Properties
    {
        _MainTex("T", 2D) = "white"{}
        _StencilRef ("Stencil Ref", int) = 2
        [Enum(UnityEngine.Rendering.CompareFunction)] _StencilCompare("Stencil Comp",int) = 4
        [Enum(UnityEngine.Rendering.StencilOp)] _StencilOp("Stencil Op",Float)=2
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Transparent"}
        LOD 100
        Pass
        {
            Stencil {
                Ref [_StencilRef] //2
                Comp [_StencilCompare] //always
                Pass [_StencilOp] //replace
            }
            
            CGPROGRAM
            sampler2D _MainTex;
            #pragma vertex vert_img
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            fixed4 frag (v2f_img i) : SV_Target
            {
                fixed4 c = tex2D(_MainTex, i.uv);
                // 不要なピクセル(透明なピクセル)を破棄させてステンシルテストを実行させない
                clip(c.a - 0.5);
                return c;
            }
            ENDCG
        }
    }
}