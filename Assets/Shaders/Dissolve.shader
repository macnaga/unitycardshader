﻿Shader "Custom/Dissolve" 
{
    Properties 
    {
        _MainTex ("Main Texture", 2D)            = "grey" {}
        _DistortionTex ("Distortion Texture(RG)", 2D)            = "grey" {}
        _DissolveBlend ("Dissolve Blending", Range(0, 1)) = 0
		_DissolveBorderWidth ("Dissolve Border width", Range(0, 100)) = 10
		_DissolveGlowColor ("Dissolve Glow color", Color) = (1,1,1,1)
		_DissolveGlowStrength ("Dissolve Glow strength", Range(0, 5)) = 1
    }
    
    SubShader 
    {
        Tags {"Queue"="Transparent" "RenderType"="Transparent" }
        Cull Back 
        Blend SrcAlpha OneMinusSrcAlpha
        
        Pass {

            CGPROGRAM
           #pragma vertex vert
           #pragma fragment frag
            
           #include "UnityCG.cginc"

            struct appdata {
                half4 vertex                : POSITION;
                half4 texcoord              : TEXCOORD0;
            };
                
            struct v2f {
                half4 vertex                : SV_POSITION;
                half2 uv                    : TEXCOORD0;
                half4 grabPos               : TEXCOORD1;
            };
            
            sampler2D _MainTex;
            half4 _MainTex_ST;
            sampler2D _DistortionTex;
            half4 _DistortionTex_ST;
            sampler2D _GrabPassTexture;
            half _DistortionPower;

			uniform sampler2D _DissolveTex; uniform float4 _DissolveTex_ST;
			uniform half _DissolveBlend;
			uniform half _DissolveBorderWidth;
			uniform half4 _DissolveGlowColor;
			uniform half _DissolveGlowStrength;
        
            inline half3 DissolveBlending(half dissolveChannel, half borderWidth, half blendAmount, half3 glowColor, half glowStrenght)
            {
                half remap = (blendAmount * -1) + 1;
                half extended = pow(((dissolveChannel * remap) * 5.0), 35.0);
                clip(clamp(extended, 0, 1) - 0.5);
                half DissolveCondition_A = step(borderWidth, extended);
                half DissolveCondition_B = step(extended, borderWidth);
                return glowColor * (lerp(DissolveCondition_B * 1, float(0), DissolveCondition_A * DissolveCondition_B) * glowStrenght);
            }

            v2f vert (appdata v)
            {
                v2f o                   = (v2f)0;
                o.vertex                = UnityObjectToClipPos(v.vertex);
                o.uv                    = TRANSFORM_TEX(v.texcoord, _DistortionTex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                half4 _MainTex_var = tex2D(_MainTex, TRANSFORM_TEX(i.uv, _MainTex));
                half4 _DissolveTex_var = tex2D(_DistortionTex, TRANSFORM_TEX(i.uv, _DistortionTex));
                half3 dissolveCol = DissolveBlending(_DissolveTex_var.r, _DissolveBorderWidth, _DissolveBlend, _DissolveGlowColor.rgb, _DissolveGlowStrength);
				half3 finalColor = _MainTex_var + dissolveCol;
				return fixed4(finalColor, _MainTex_var.a);
            }
            ENDCG
        }
    }
}