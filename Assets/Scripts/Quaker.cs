﻿using UnityEngine;
using System.Collections;

public class Quaker : MonoBehaviour
{
    public Transform targetTransform;
    public float shakeDuration = 0f;
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    Vector3 originalPos;

    void Awake()
    {
        if (targetTransform == null)
        {
            targetTransform = this.transform;
        }
    }

    void OnEnable()
    {
        originalPos = targetTransform.localPosition;
    }

    void Update()
    {
        if (shakeDuration > 0)
        {
            targetTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            targetTransform.localPosition = originalPos;
        }
    }
}