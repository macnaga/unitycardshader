﻿using UnityEngine;

[RequireComponent (typeof(Camera))]
public class DisintegrationImageEffect : MonoBehaviour
{
    #region Fields

    [SerializeField]
    [Range (-2000, 2000)]
    private float StrengthX = 1f;

    [SerializeField]
    [Range (0, 2000)]
    private float StrengthY = 1f;

    [SerializeField]
    [Range (0, 1)]
    private float BeginY = 0.9f;

    [SerializeField]
    [Range (0, 1)]
    private float EndY = 1f;

    [SerializeField]
    private float StrengthXSpeed = 0f;

    [SerializeField]
    private float StrengthYSpeed = 0.1f;

    [SerializeField]
    private float EndYSpeed = 0.001f;

    [SerializeField]
    [Range (0, 1)]
    private float IntervalY = 0.1f;

    #endregion

    private Material m_Material;

    #region Properties

    public string ShaderName {
        get { return "Custom/DisintegrationShader"; }
    }

    #endregion

    #region Methods

    void Awake ()
    {
        Shader shader = Shader.Find (ShaderName);
        m_Material = new Material (shader);
    }

    void OnRenderImage (RenderTexture source, RenderTexture destination)
    {
        UpdateMaterial ();
        Graphics.Blit (source, destination, m_Material);
    }

    void UpdateMaterial ()
    {
        m_Material.SetFloat ("_StrengthX", StrengthX);
        m_Material.SetFloat ("_StrengthY", StrengthY);
        m_Material.SetFloat ("_BeginY", BeginY);
        m_Material.SetFloat ("_EndY", EndY);
    }

    void Update ()
    {
        // ピクセルの散らし具合を増していく。
        StrengthX += StrengthXSpeed;
        StrengthY += StrengthYSpeed;
        EndY += EndYSpeed;

        // UVがBeginYからEndYまでの領域のピクセルを散らす。
        float initialY = 1f - IntervalY;
        for (float t = initialY; t > 0f; t -= IntervalY) {
            if (EndY >= t) {
                if (BeginY != t) {
                    StrengthX = 0f;
                    StrengthY = 0f;
                }
                BeginY = t;
                break;
            }
        }
    }

    #endregion
}
