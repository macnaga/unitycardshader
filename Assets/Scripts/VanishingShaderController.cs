﻿using UnityEngine;

[ExecuteInEditMode]
public class VanishingShaderController : MonoBehaviour
{
    [SerializeField] float StrengthX = 0f;

    [SerializeField] float StrengthY = 0f;

    [SerializeField] Material m_Material;
    [SerializeField] float Alpha = 1f;

    void Update()
    {
        UpdateMaterial();
    }

    void UpdateMaterial()
    {
        m_Material.SetFloat("_StrengthX", StrengthX);
        m_Material.SetFloat("_StrengthY", StrengthY);
        m_Material.SetFloat("_Alpha", Alpha);
    }
}
